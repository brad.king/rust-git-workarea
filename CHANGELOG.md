# v4.1.0 (unreleased)

  * Arguments taking `AsRef<Path>` are now `Into<PathBuf>` if the
    implementation ends up owning the parameter anyays.

# v4.0.0

  * The `MergeCommand::author_date` method now takes a `DateTime<Utc>` rather
    than a reference.
  * The `GitWorkArea::checkout` method now requires a mutable reference. This
    is because checking out locks the index which means that it cannot be
    shared while performing this action.
  * The lifetime for merging has been moved to the `MergeCommand` structure.
    This prevents the command from outliving the workarea it uses for conflict
    files.
  * `error-chain` is no longer used. Instead, custom error types are used. This
    means that errors from this crate no longer allocate (except where a
    `String` needs to be stored for messaging purposes).
  * `Into<String>` is used for `CommitId` and `Identity` construction methods.

# v3.1.2

  * More submodule name/path mismatch fixes.

# v3.1.1

  * Submodules which have different names and paths are now properly handled.

# v3.1.0

  * Added support for "update merges". This is equivalent to using the `ours`
    merge strategy.
  * Checking out files now accepts an iterator over pathspecs.

# v3.0.0

  * Bump `chrono` from `0.3` to `0.4`. This is required for compatibility with
    `serde-1.0`.

# v2.0.0

  * Support checking out files into a prepared workarea.
  * Add `MergeCommand` to handle creation of the merge commit.
  * `GitContext::fetch` now supports an iterator over refnames.
  * Renamed `PreparedGitWorkArea` to `GitWorkArea`.

# v1.0.1

  * Documentation fixes.

# v1.0.0

  * Initial stable release.
